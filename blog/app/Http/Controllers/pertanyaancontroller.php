<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pertanyaancontroller extends Controller
{
    public function index(){
        $data = DB::table('pertanyaan')->get();
        return view ('day15crud.pertanyaan', compact('data'));
    }

    public function create(){
        return view ('day15crud.create');
        
    }

    public function store(request $request){

        // dd($request->all());
        $request-> validate ([
             "judulpertanyaan" => 'required',
             "isipertanyaan"=>'required'
         ]);

        DB::table('pertanyaan')->insert(
             ["judul" => $request["judulpertanyaan"], 
             "isi" => $request["isipertanyaan"]
             ]);
        
            return redirect('/pertanyaan') ->with('success','Pertanyaan Berhasil Disimpan');
    }

    public function show($id) {
        $data = DB:: table('pertanyaan')->where('id',$id)->first();
        return view('day15crud.show',compact('data'));
    }

    public function edit($id) {
        $data = DB:: table('pertanyaan')->where('id',$id)->first();
        return view('day15crud.edit',compact('data'));
    }

    public function update($id, request $request) {
        $request-> validate ([
            "judulpertanyaan" => 'required',
            "isipertanyaan"=>'required'
        ]);

        $data = DB:: table('pertanyaan')->where('id',$id)
                    ->update([
                        'judul'=>$request['judulpertanyaan'],
                        'isi'=>$request['isipertanyaan']
                    ]);
        return redirect('/pertanyaan')->with('success','Data Berhasil Diubah !');
    }

    public function destroy($id) {
        $data = DB:: table('pertanyaan')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('success','Data Berhasil Dihapus !');
    }
}
