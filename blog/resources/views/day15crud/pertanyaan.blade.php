@extends('adminlte.master')

@section('content')
                <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success">
                {{session('success')}}
                </div>
                @endif
                <a class="btn btn-primary mb-3" href="/pertanyaan/create"> Buat Pertanyaan Baru </a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">ID</th>
                      <th>Judul Pertanyaan</th>
                      <th>Isi Pertanyaan</th>
                      <th style="text-align:center; width:400px;">Actions </th>
                    </tr>
                  </thead>
                  <tbody>
                  @forelse($data as $key => $data1)
                  <tr>
                    <td> {{$key + 1}} </td>
                    <td>{{$data1->judul}}</td>   
                    <td>{{$data1->isi}}</td>
                    <td style="display:flex; justify-content: center;">
                      <a href="/pertanyaan/{{$data1->id}}" class="btn btn-info btn sm ml-2" > Show Detail </a>
                      <a href="/pertanyaan/{{$data1->id}}/edit" class="btn btn-default btn sm ml-2" > Edit </a>
                      <form action="/pertanyaan/{{$data1->id}}" method ="post"> 
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="Delete" class="btn btn-danger btn sm ml-2" >
                      </form> 
                    </td>
                  </tr>
                  @empty
                  <tr>
                  <td colspan="4" align ="center"> Tidak Ada Pertanyaan </td>
                  <tr>
                  @endforelse
                  
                  </tbody>
                </table>
              </div>

@endsection