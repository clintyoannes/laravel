<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@home');
// Route::get('/registration', 'AuthController@registration');
// Route::post('/welcomeuser', 'AuthController@welcome');

Route::view('/', 'welcome');

Route::get('/pertanyaan', 'pertanyaancontroller@index');
Route::get('/pertanyaan/create', 'pertanyaancontroller@create');
Route::post('/pertanyaan', 'pertanyaancontroller@store');
Route::get('/pertanyaan/{id}', 'pertanyaancontroller@show');
Route::get('/pertanyaan/{id}/edit', 'pertanyaancontroller@edit');
Route::put('/pertanyaan/{id}', 'pertanyaancontroller@update');
Route::delete('/pertanyaan/{id}', 'pertanyaancontroller@destroy');