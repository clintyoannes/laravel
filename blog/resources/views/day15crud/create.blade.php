@extends('adminlte.master')

@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Membuat Pertanyaan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<div class = "ml-3 mt-2 mr-2">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Pertanyaan Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method ="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judulpertanyaan" name="judulpertanyaan" value="{{old('judulpertanyaan')}}" placeholder="Masukan Judul">
                  </div>
                  @error('judulpertanyaan')
                    <div class="alert alert-danger">Field is required</div>
                    @enderror
                  <div class="form-group">
                    <label for="exampleInputPassword1">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isipertanyaan" name="isipertanyaan"  value="{{old('isipertanyaan')}}"placeholder="Masukan Pertanyaan">
                  </div>
                  @error('isipertanyaan')
                 <div class="alert alert-danger">Field is required</div>
                  @enderror
                </div>
                

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>
</div>

 @endsection